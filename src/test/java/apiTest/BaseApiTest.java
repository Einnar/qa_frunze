package apiTest;

import API.controllers.BaseAsiaController;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeSuite;

public class BaseApiTest {

    protected BaseAsiaController baseAsiaController = new BaseAsiaController();

    @BeforeSuite
    public void beforeSuite(){
        System.out.println("=============== Start Api Test ===============");
    }

    @AfterSuite
    public void afterSuite(){
        System.out.println("=============== End Api Test ===============");
    }

}
