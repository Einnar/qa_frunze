package apiTest;

import API.asserts.ApiAsserts;
import API.controllers.AsiaController;
import API.pojo.user.checkOtp.CheckOtp;
import API.pojo.user.isVerified.IsVerified;
import API.pojo.user.mbankAuth.MBankAuth;
import API.pojo.user.oneTimeCardQrCode.OneTimeCardQrCode;
import API.pojo.user.paymentCard.PaymentCard;
import API.pojo.user.selectDiscountCard.SelectDiscountCard;
import API.pojo.user.sendCode.SendCode;
import API.pojo.user.bonusCard.BonusCard;
import API.pojo.user.users.User;
import API.pojo.user.updateProfile.UpdateProfile;
import io.restassured.response.Response;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.util.ArrayList;


import static API.pojo.user.checkOtp.CheckOtpRequest.getCheckOtp;
import static API.pojo.user.mbankAuth.MBankAuthRequest.getMBankAuth;
import static API.pojo.user.selectDiscountCard.SelectDiscountCardRequest.getSelectDiscountCard;
import static API.pojo.user.sendCode.SendCodeRequest.getSendCode;
import static API.pojo.user.updateProfile.UpdateProfileRequest.getUpdateProfile;
import static java.net.HttpURLConnection.HTTP_CREATED;
import static java.net.HttpURLConnection.HTTP_OK;

@Getter
@Slf4j
public class UserApiTest extends BaseApiTest {


    private AsiaController asiaController;
    private Response response;
    private User user;
    private BonusCard bonusCard;
    private PaymentCard paymentCard;
    private OneTimeCardQrCode timeCardQrCode;
    private IsVerified isVerified;
    private final ArrayList<User> userList = new ArrayList<>();


    @BeforeClass
    public void beforeClass() {
        asiaController = baseAsiaController.getAsiaController();
    }

    @Test(priority = 1)
    public void postSendCodeTest() {
        final SendCode sendCod = getSendCode();
        asiaController.postRequest(sendCod);
        ApiAsserts.assertThatResponse(asiaController.getResponse())
                .isCorrectHttpStatusCode(HTTP_CREATED);

    }

    @Test(dependsOnMethods = "postSendCodeTest")
    public void postCheckOtpTest() {
        final CheckOtp checkOtp = getCheckOtp();
        asiaController.postRequest(checkOtp);
        ApiAsserts.assertThatResponse(asiaController.getResponse())
                .isCorrectHttpStatusCode(HTTP_OK);
    }

    @Test(dependsOnMethods = "postCheckOtpTest")
    public void getUserTest() {
        user = asiaController.getRequestUser().as(User.class);
        userList.add(user);
        for (int i = 0; i < userList.size(); i++) {
            System.out.println("UserList: " + userList.get(i).toString());
        }
        ApiAsserts.assertThatResponse(asiaController.getResponse())
                .isCorrectHttpStatusCode(HTTP_OK);
    }


    @Test(dependsOnMethods = "getUserTest")
    public void getBonusCardTest() {
        bonusCard = asiaController.getRequestDiscount().as(BonusCard.class);
        ApiAsserts.assertThatResponse(asiaController.getResponse())
                .isCorrectHttpStatusCode(HTTP_OK);
    }


    @Test(dependsOnMethods = "getBonusCardTest")
    public void postUpdateProfile() {
        final UpdateProfile updateProfile = getUpdateProfile();
        asiaController.postRequestUpdateProfile(updateProfile);
        ApiAsserts.assertThatResponse(asiaController.getResponse())
                .isCorrectHttpStatusCode(HTTP_CREATED);
    }

    @Test(dependsOnMethods = "postUpdateProfile")
    public void postSelectDiscountCardTest(){
        final SelectDiscountCard selectDiscountCard = getSelectDiscountCard();
        asiaController.postRequestSelectDiscountCard(selectDiscountCard);
        ApiAsserts.assertThatResponse(asiaController.getResponse())
                .isCorrectHttpStatusCode(HTTP_OK);
    }


    @Test(dependsOnMethods = "postSelectDiscountCardTest")
    public void getPaymentCardTest(){
        paymentCard = asiaController.getRequestPaymentCard().as(PaymentCard.class);
        ApiAsserts.assertThatResponse(asiaController.getResponse())
                .isCorrectHttpStatusCode(HTTP_OK);
    }


    @Test(dependsOnMethods = "getPaymentCardTest")
    public void gettOneTimeCardQrCodeTest(){
        timeCardQrCode = asiaController.getRequestOneTimeCardQrCode().as(OneTimeCardQrCode.class);
        ApiAsserts.assertThatResponse(asiaController.getResponse())
                .isCorrectHttpStatusCode(HTTP_OK);
    }

    @Test(dependsOnMethods = "gettOneTimeCardQrCodeTest")
    public void postmbankAuth(){
        final MBankAuth mBankAuth = getMBankAuth();
        asiaController.postMbankAuth(mBankAuth);
        ApiAsserts.assertThatResponse(asiaController.getResponse())
                .isCorrectHttpStatusCode(HTTP_OK);
    }


    @Test(dependsOnMethods = "postmbankAuth")
    public void getIsVerifiedTest(){
        isVerified = asiaController.getRequestIsVerified().as(IsVerified.class);
        ApiAsserts.assertThatResponse(asiaController.getResponse())
                .isCorrectHttpStatusCode(HTTP_OK);
    }

}
