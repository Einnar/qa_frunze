package API.asserts;

import io.restassured.response.Response;
import io.restassured.response.ResponseBody;
import lombok.extern.slf4j.Slf4j;
import org.assertj.core.api.Assertions;

/**
 * @author Einar Toktogonov
 */
@Slf4j
public class ApiAsserts {

    private final Response response;

    private ResponseBody responseBody;


    public ApiAsserts(Response response){
        this.response = response;
    }

    public static ApiAsserts assertThatResponse(Response response){
        log.info("Start assert response...");
        new ApiAsserts(response);
        return new ApiAsserts(response);
    }


    public ApiAsserts isCorrectHttpStatusCode(Integer expectedStatusCode){
        Assertions.assertThat(this.response.getStatusCode())
                .isEqualTo(expectedStatusCode)
                .withFailMessage("Response code is incorrect, Expected %s, but found %s",expectedStatusCode,this.response.getStatusCode());
        log.info("Status code is correct: Actual {} Expected {}", this.response.getStatusCode(),expectedStatusCode);
        return this;
    }

}
