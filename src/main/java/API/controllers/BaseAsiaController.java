package API.controllers;

import API.driverProvaiders.ConfigReader;
import lombok.Getter;


@Getter
public class BaseAsiaController {

    private final AsiaController asiaController;

    public BaseAsiaController(){
        this.asiaController = new AsiaController(ConfigReader.getProperty("url"));
    }
}
