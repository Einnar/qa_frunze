package API.controllers;

import API.pojo.user.checkOtp.CheckOtp;
import API.pojo.user.mbankAuth.MBankAuth;
import API.pojo.user.selectDiscountCard.SelectDiscountCard;
import API.pojo.user.sendCode.SendCode;
import API.pojo.user.updateProfile.UpdateProfile;
import API.request.RequestManager;
import API.utils.JsonUtils;
import io.restassured.response.Response;

import static API.asiaApi.EndPoint.*;

public class AsiaController extends RequestManager {


    public AsiaController(String url) {
        super(url, BASE_HEADERS);
    }

    public Response postRequest(SendCode sendCode){
        return this.response = super.post(getEndPoint(API,USERS,SEND_CODE)
                ,JsonUtils.convertObjectToJson(sendCode));
    }

    public Response postRequest(CheckOtp checkOtp){
        return this.response = super.post(getEndPoint(API,USERS,CHECK_OTP)
                ,JsonUtils.convertObjectToJson(checkOtp));
    }

    public Response  getRequestUser(){
        return this.response = super.get(getEndPoint(API,USERS,ME));
    }

    public Response getRequestDiscount(){
        return this.response = super.get(getEndPoint(API,USERS,DISCOUNT_CARD));
    }

    public Response postRequestUpdateProfile(UpdateProfile updateProfile){
        return this.response = super.post(getEndPoint(API, USERS,UPDATE_PROFILE)
                ,JsonUtils.convertObjectToJson(updateProfile));
    }

    public Response postRequestSelectDiscountCard(SelectDiscountCard selectDiscountCard){
        return this.response = super.post(getEndPoint(API,USERS,SELECT_DISCOUNT_CARD)
                ,JsonUtils.convertObjectToJson(selectDiscountCard));
    }

    public Response getRequestPaymentCard(){
        return this.response = super.get(getEndPoint(API,USERS,PAYMENT_CARD));
    }

    public Response getRequestOneTimeCardQrCode(){
        return this.response = super.get(getEndPoint(API,USERS,ONE_TOME_CARD_QR_CODE));
    }

    public Response postMbankAuth(MBankAuth mBankAuth){
        return this.response = super.post(getEndPoint(API,USERS,MBANK_AUTH)
        ,JsonUtils.convertObjectToJson(mBankAuth));
    }

    public Response getRequestIsVerified(){
        return this.response = super.get(getEndPoint(API,USERS,IS_VERIFIED));
    }
}
