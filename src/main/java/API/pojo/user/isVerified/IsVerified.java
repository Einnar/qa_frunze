package API.pojo.user.isVerified;


import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class IsVerified {

    @JsonProperty("is_verified")
    private Boolean isVerified;
}
