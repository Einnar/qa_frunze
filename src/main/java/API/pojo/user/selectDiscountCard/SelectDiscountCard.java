package API.pojo.user.selectDiscountCard;


import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.Data;
import lombok.Getter;

@Data
@Builder
@Getter
public class SelectDiscountCard {

    @JsonProperty("card_number")
    private String cardNumber;
}
