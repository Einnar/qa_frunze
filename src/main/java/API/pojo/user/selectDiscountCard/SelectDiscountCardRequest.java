package API.pojo.user.selectDiscountCard;

import net.datafaker.Faker;

public class SelectDiscountCardRequest {

    public static SelectDiscountCard getSelectDiscountCard(){
        final Faker faker = new Faker();
        return SelectDiscountCard.builder()
                .cardNumber("23")
                .build();
    }
}
