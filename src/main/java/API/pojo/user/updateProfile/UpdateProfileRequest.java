package API.pojo.user.updateProfile;

import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import net.datafaker.Faker;

@Data
@Builder
@Getter
public class UpdateProfileRequest {

    public static UpdateProfile getUpdateProfile(){
        final Faker faker = new Faker();
        return UpdateProfile.builder()
                .gender(true)
                .firstName("Einar")
                .lastName("Toktogonov")
                .dateOfBirth("2003-01-12")
                .email("einar.kg73@mail.ru")
                .phoneNumber("+996500882942")
                .build();
    }
}
