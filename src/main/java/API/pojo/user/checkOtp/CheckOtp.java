package API.pojo.user.checkOtp;


import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.Data;
import lombok.Getter;

@Getter
@Data
@Builder
public class CheckOtp {

    private String code;
    @JsonProperty("phone_number")
    private String phoneNumber;
    @JsonProperty("notification_token")
    private String notificationToken;
    @JsonProperty("subscribed_to_news")
    private Boolean subscribedToNews;
}
