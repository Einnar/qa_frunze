package API.pojo.user.checkOtp;

import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import net.datafaker.Faker;


@Getter
@Data
@Builder
public class CheckOtpRequest {

    public static CheckOtp getCheckOtp(){
        final Faker faker = new Faker();
        return CheckOtp.builder()
                .code("7777")
                .phoneNumber("+996500882942")
                .notificationToken("re")
                .subscribedToNews(true)
                .build();
    }
}
