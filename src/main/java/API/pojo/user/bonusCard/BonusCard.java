package API.pojo.user.bonusCard;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;


@AllArgsConstructor
@NoArgsConstructor
@Builder
@Data
public class BonusCard {

    @JsonProperty("discount_card")
    private Integer discountCard;
    private String number;
    @JsonProperty("is_virtual")
    private Boolean isVirtual;
    @JsonProperty("is_active")
    private Boolean isActive;
    @JsonProperty("card_type")
    private String cardType;

}
