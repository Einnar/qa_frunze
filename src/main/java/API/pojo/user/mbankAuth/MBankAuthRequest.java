package API.pojo.user.mbankAuth;


import lombok.Builder;
import lombok.Data;

import net.datafaker.Faker;


@Data
@Builder
public class MBankAuthRequest {

    public static MBankAuth getMBankAuth(){
        Faker faker = new Faker();
        return MBankAuth.builder()
                .token("re")
                .build();
    }
}
