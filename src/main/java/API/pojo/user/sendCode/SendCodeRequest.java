package API.pojo.user.sendCode;

import lombok.Builder;
import lombok.Data;
import net.datafaker.Faker;


@Data
@Builder
public class SendCodeRequest {

    public static SendCode getSendCode(){
        final Faker faker = new Faker();
        return SendCode.builder()
                .phoneNumber("+996500882942")
                .build();
    }
}
