package API.pojo.user.sendCode;


import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.Data;
import lombok.Getter;


@Getter
@Builder
@Data
public class SendCode {

    @JsonProperty("phone_number")
    private String phoneNumber;

}
