package API.pojo.user.paymentCard;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

import java.util.ArrayList;


@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class PaymentCard {

   @JsonProperty("default")
   private ArrayList<Card> cards;
   private String bonus;
}

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
class Card {

   @JsonProperty("card_id")
   private String cardId;
   @JsonProperty("card_number")
   private String cardNumber;
   @JsonProperty("card_type")
   private String cardType;
}