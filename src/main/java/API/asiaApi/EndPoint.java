package API.asiaApi;

import io.restassured.http.Header;
import io.restassured.http.Headers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class EndPoint {

    public static final String API = "api";
    public static final String USERS = "users";
    public static final String SEND_CODE = "sendCode/";
    public static final String CHECK_OTP = "checkOtp/";
    public static final String ME = "me";
    public static final String DISCOUNT_CARD = "discountCard";
    public static final String UPDATE_PROFILE = "updateProfile/";
    public static final String SELECT_DISCOUNT_CARD = "selectDiscountCard/";
    public static final String PAYMENT_CARD = "paymentCards";
    public static final String ONE_TOME_CARD_QR_CODE = "oneTimeCardQRCode";
    public static final String MBANK_AUTH = "mbankAuth/";
    public static final String IS_VERIFIED = "is_verified";

    public static Map<String, String> BASE_HEADERS = new HashMap<>(){{
        put("accept", "application/json");
        put("imei", "some_random_uuid");
        put("Content-Type", "application/json");
        put("X-CSRFToken", "X28CRLeUP6B7c1OP1gtTRYF26N9v8PXNEp95ijLmW4jRo9IM4CV6rQCxMh672jZ6");
    }};
}
