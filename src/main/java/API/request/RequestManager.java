package API.request;


import API.asiaApi.EndPoint;
import io.restassured.authentication.PreemptiveOAuth2HeaderScheme;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;

import java.util.HashMap;
import java.util.Map;

import static API.asiaApi.EndPoint.USERS;
import static io.restassured.RestAssured.given;

@Slf4j
@Data
public abstract class RequestManager {

    protected String url;
    protected Map<String, String> headers;
    public Response response;
    protected RequestSpecification specification;


    public RequestManager (String url, Map<String, String> headers){
        this.headers = headers;
        this.url = url;
        PreemptiveOAuth2HeaderScheme authScheme = new PreemptiveOAuth2HeaderScheme();
        authScheme.setAccessToken("eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VyX2lkIjo1NjEyNTYsInVzZXJuYW1lIjoiKzk5NjUwMDg4Mjk0MiIsImV4cCI6NTQ3ODIzMzk4MCwiZW1haWwiOiJ0b2t0b2dvbm92ZWluYXJAZ21haWwuY29tIiwicGhvbmVfbnVtYmVyIjoiKzk5NjUwMDg4Mjk0MiIsIm9yaWdfaWF0IjoxNjkzOTEzOTgwfQ.nFVaPuG4DmQiE8MydsOr9_KO-nZCNC9iqGguf8TtJg8");
        specification = new RequestSpecBuilder()
                .setContentType(ContentType.JSON)
                .setAuth(authScheme)
                .setBaseUri(this.url)
                .setAccept(ContentType.JSON)
                .addHeaders(this.headers)
                .build();
        specification.log();
    }

    public static final String SLASH = "/";
    protected static String[] DEFAULT_ENDPOINTS = {""};

    public static String getEndPoint(String... args){
        StringBuilder endPoint = new StringBuilder();
        for (String arg : args){
            endPoint.append(arg).append(SLASH);
        }
        return endPoint.substring(0,endPoint.length() -1);
    }

    public static String formatParameters(HashMap<String,String> parameters){
        StringBuilder query = new StringBuilder("?");
        for (Map.Entry<String, String> entry : parameters.entrySet()){
            query.append(entry.getKey() + "=" + entry.getValue() + "&");
        }
        return query.deleteCharAt(query.length() - 1).toString();
    }

    public RequestManager logResponse(){
        log.warn("response is:");
        log.warn(getResponse().getBody().asString());
        log.warn(String.valueOf(getResponse().getStatusCode()));
        return this;
    }

    public Response get(String endPoint){
        log.info("Perform get request: {}", endPoint);
        this.response = given()
                .spec(specification)
                .get(endPoint);
        logResponse();
        return this.response;
    }

    public Response post(String endPoint, String body){
        log.info("Perform post request: {}", endPoint);
        log.info("Body is: {}", body);
        this.response = given()
                .spec(specification)
                .body(body)
                .post(endPoint);
        logResponse();
        return this.response;
    }


    public Response put(String endPoint, String body){
        log.info("Perform put request: {}", endPoint);
        log.info("Body is: {}", body);
        this.response = given()
                .spec(specification)
                .body(body)
                .put(endPoint);
        logResponse();
        return this.response;
    }

    public Response patch(String endPoint, String body){
        log.info("Perform patch request: {}", endPoint);
        log.info("Body is: {}", body);
        this.response = given()
                .spec(specification)
                .body(body)
                .patch(endPoint);
        logResponse();
        return this.response;
    }

    public Response delete(String endPoint){
        log.info("Perform delete request: {}", endPoint);
        this.response = given()
                .spec(specification)
                .delete(endPoint);
        logResponse();
        return this.response;
    }

    public Response delete(String endPoint, String body){
        log.warn("Perform delete request: {}",endPoint);
        log.warn("Body is: {}",body);
        this.response = given()
                .spec(specification)
                .body(body)
                .delete(endPoint);
        logResponse();
        return this.response;
    }

}
